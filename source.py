"""
module exemple de parsing de commande avec fissix (lib2to3)
"""
from unittest import TestCase, main
from fissix import pygram, pytree
from fissix.pgen2 import driver


class SmokeTest(TestCase):
    """Classe test permettant de juger de la correction du parsing
    """
    def setUp(self):
        self.grammar = pygram.python_grammar
        self.driver = driver.Driver(self.grammar, convert=pytree.convert)

    def test_parse_string(self):
        """
        simple parsing de commandes
        """
        code = """
from foo import bar
value = 123
print(bar(f"123"))
        """
        tree = self.driver.parse_string(code)
        assert isinstance(tree, pytree)

if __name__ == "__main__":
    main()
